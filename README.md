# Estiam-k8s

Projet final

Commencez par effectuer la commande chmod +x pour chaques scripts
- chmod +x install-minkube.sh
- chmod +x docker-compose/estiam/minikube-pods.sh
- chmod +x LancerDashboard.sh

Puis les executer dans l'ordre
- sudo ./install-minkube.sh
- sudo ./docker-compose/estiam/minikube-pods.sh
- sudo ./LancerDashboard.sh
