#!/bin/bash
#installation de kubectl en téléchargeant une version binaire statique
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl
chmod +x ./kubectl

# déplacer kubectl
mv ./kubectl /usr/local/bin/kubectl

#installtion de Minikube en téléchargeant une version binaire statique
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube

#création d'un dossier  
mkdir -p /usr/local/bin/
install minikube /usr/local/bin/
#lancement de minikube
minikube start --vm-driver=none
#verification
minikube status
