#!/bin/bash

curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
chmod +x /usr/local/bin/docker-compose
docker-compose -v
kubectl create -f ~/estiam-k8s/estiam-k8s-project/ns-estiam-k8s-project.yml
kubectl create -f ~/estiam-k8s/estiam-k8s-project/service-estiam-k8s-project.yml
kubectl create -f ~/estiam-k8s/estiam-k8s-project/deployment-estiam-k8s-project.yml
cd docker-compose/estiam/
sudo docker-compose ps --services
cd ~/estiam-k8s/
